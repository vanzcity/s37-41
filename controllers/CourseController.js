const Course = require("../models/Course")

// Get all ACTIVE Courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(active_courses => {
		return active_courses
	})
}

// Create new course
module.exports.createCourse = (request_body) => {
	let new_course = new Course ({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price
	})

	return new_course.save().then((created_course, error) =>{
		if(error){
			return error
		}

		return 'Course created successfully!'
	})
}

